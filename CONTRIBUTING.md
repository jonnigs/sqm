Please contribute to this project if you want to add features or changes.

Keep in mind the coding rules and the software quality management plan found in the [Wiki pages](https://gitlab.com/jonnigs/sqm/-/wikis/home)