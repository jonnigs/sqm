package is.hi.hbv501g.agb.AGB.Entities;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class AdventurerTest {
  @Test
  public void testGetId() {
    Adventurer testAdventurer = new Adventurer("hashedpasswordXADFDafdk", "adv@adv.is", "AdventureMan2020");
    assertEquals(0, testAdventurer.getId());
  }

  @Test
  public void testSetId() {
    Adventurer testAdventurer = new Adventurer("hashedpasswordXADFDafdk", "adv@adv.is", "AdventureMan2020");
    testAdventurer.setId(2);
    assertEquals(2, testAdventurer.getId());
  }

  @Test
  public void testGetPasswordHashed() {
    Adventurer testAdventurer = new Adventurer("hashedpasswordXADFDafdk", "adv@adv.is", "AdventureMan2020");
    assertEquals("hashedpasswordXADFDafdk", testAdventurer.getPasswordHashed());
  }

  @Test
  public void testSetPasswordHashed() {
    Adventurer testAdventurer = new Adventurer("hashedpasswordXADFDafdk", "adv@adv.is", "AdventureMan2020");
    testAdventurer.setPasswordHashed("AnotherHashedpw");
    assertEquals("AnotherHashedpw", testAdventurer.getPasswordHashed());
  }

  @Test
  public void testGetEmail() {
    Adventurer testAdventurer = new Adventurer("hashedpasswordXADFDafdk", "adv@adv.is", "AdventureMan2020");
    assertEquals("adv@adv.is", testAdventurer.getEmail());
  }

  @Test
  public void testSetEmail() {
    Adventurer testAdventurer = new Adventurer("hashedpasswordXADFDafdk", "adv@adv.is", "AdventureMan2020");
    testAdventurer.setEmail("another@email.com");
    assertEquals("another@email.com", testAdventurer.getEmail());
  }

  @Test
  public void testGetDisplayName() {
    Adventurer testAdventurer = new Adventurer("hashedpasswordXADFDafdk", "adv@adv.is", "AdventureMan2020");
    assertEquals("AdventureMan2020", testAdventurer.getDisplayName());
  }

  @Test
  public void testSetDisplayName() {
    Adventurer testAdventurer = new Adventurer("hashedpasswordXADFDafdk", "adv@adv.is", "AdventureMan2020");
    testAdventurer.setDisplayName("SafariMan");
    assertEquals("SafariMan", testAdventurer.getDisplayName());
  }

}
