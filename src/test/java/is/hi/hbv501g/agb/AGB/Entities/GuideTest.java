package is.hi.hbv501g.agb.AGB.Entities;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class GuideTest {
    @Test
    public void testGetId() {
        Guide testGuide = new Guide(1, "Nýr gæd", "Ferð um ísland");
        assertEquals(1, testGuide.getId());
    }

    @Test
    public void testSetId() {
        Guide testGuide = new Guide(1, "Nýr gæd", "Ferð um ísland");
        testGuide.setId(2);
        assertEquals(2, testGuide.getId());
    }

    @Test
    public void testGetTitle() {
        Guide testGuide = new Guide(1, "Nýr gæd", "Ferð um ísland");
        assertEquals("Nýr gæd", testGuide.getTitle());
    }

    @Test
    public void testSetTitle() {
        Guide testGuide = new Guide(1, "Nýr gæd", "Ferð um ísland");
        testGuide.setTitle("Annar titill");
        assertEquals("Annar titill", testGuide.getTitle());
    }

    @Test
    public void testGetDescription() {
        Guide testGuide = new Guide(1, "Nýr gæd", "Ferð um ísland");
        assertEquals("Ferð um ísland", testGuide.getDescription());
    }

    @Test
    public void testSetDescription() {
        Guide testGuide = new Guide(1, "Nýr gæd", "Ferð um ísland");
        testGuide.setDescription("Önnur lýsing");
        assertEquals("Önnur lýsing", testGuide.getDescription());
    }

    @Test
    public void testToString() {
        Guide testGuide = new Guide(1, "Nýr gæd", "Ferð um ísland");
        assertEquals("Nýr gæd", testGuide.toString());
    }

}
