package is.hi.hbv501g.agb.AGB.Entities;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ReviewTest {
    @Test
    public void testGetId() {
        Review testReview = new Review(1,"New review", "I liked this trip...", 4);
        assertEquals(1, testReview.getId());
    }

    @Test
    public void testSetId() {
        Review testReview = new Review(1,"New review", "I liked this trip...", 4);
        testReview.setId(2);
        assertEquals(2, testReview.getId());
    }

    @Test
    public void testGetTitle() {
        Review testReview = new Review(1,"New review", "I liked this trip...", 4);
        assertEquals("New review", testReview.getTitle());
    }

    @Test
    public void testSetTitle() {
        Review testReview = new Review(1,"New review", "I liked this trip...", 4);
        testReview.setTitle("A different title");
        assertEquals("A different title", testReview.getTitle());
    }

    @Test
    public void testGetDescription() {
        Review testReview = new Review(1,"New review", "I liked this trip...", 4);
        assertEquals("I liked this trip...", testReview.getDescription());
    }

    @Test
    public void testSetDescription() {
        Review testReview = new Review(1,"New review", "I liked this trip...", 4);
        testReview.setDescription("A different description");
        assertEquals("A different description", testReview.getDescription());
    }

    @Test
    public void testGetRating() {
        Review testReview = new Review(1,"New review", "I liked this trip...", 4);
        assertEquals(4, testReview.getRating());
    }

    @Test
    public void testSetRating() {
        Review testReview = new Review(1,"New review", "I liked this trip...", 4);
        testReview.setRating(3);
        assertEquals(3, testReview.getRating());
    }

}