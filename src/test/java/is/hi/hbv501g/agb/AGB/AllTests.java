package is.hi.hbv501g.agb.AGB;

import is.hi.hbv501g.agb.AGB.Entities.GuideTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GuideTest.class})
public class AllTests {
    // the class remains completely empty,
    // being used only as a holder for the above annotations
}



