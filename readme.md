# Adventurer's Guidebook

## CI/CD

[![pipeline status](https://gitlab.com/jonnigs/sqm/badges/master/pipeline.svg)](https://gitlab.com/jonnigs/sqm/-/commits/master)  
[![coverage report](https://gitlab.com/jonnigs/sqm/badges/master/coverage.svg)](https://gitlab.com/jonnigs/sqm/-/commits/master)  
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=jonnigs_sqm&metric=alert_status)](https://sonarcloud.io/dashboard?id=jonnigs_sqm)  

The project runs CI pipeline after each commit. The pipeline builds, tests and then deploys a generated webpage that includes JaCoCo code coverage report. The report can be found [here](https://jonnigs.gitlab.io/sqm/).  

## The team

This assignment was made in the fall of 2019 in the course HBV501G - Hugbúnaðarverkefni 1.  
On the team were Andrea Rakel Sigurðardóttir, Bjartur, Erling Óskar Kristjánsson & Jónas G. Sigurðsson.

## About the project

The project is a social media platform for tours in Iceland. Users can sign up, make new tours and rate/comment on tours they have been on, similar to tripadvisor.com.

The project uses Java Spring and templates the HTML with Thymeleaf templaring language

The system uses `spring-boot-starter-security` to manage user passwords so you need a username and password to run the application.

## Build system  

To build and run the system you can run the commands "mvn compile", "mvn package" and "java -jar target/AGB-0.0.1-SNAPSHOT.jar" from the command line, with the current directory set to the sqm directory (that has the pom.xml).  

This should start a local server that runs on port 8085 and can be viewed in a browser.  
